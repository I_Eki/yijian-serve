const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const serve = require('http').Server(app);
const io = require('socket.io')(serve);

app.use(bodyParser.json({ extended: true }));

// 接受小程序传过来的数据对象，并通过io转发
app.post('/translate', (req, res) => {
  io.emit('translate', req.body);
  res.send();
});

io.on('connection', socket => {
  socket.emit('msg', 'Hello, OCR!');
});

const port = 830;

serve.listen(port, () => {
  console.log('服务已启动，监听端口830 . . ');
});
